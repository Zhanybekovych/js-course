/*
  Васи положил 12 000 долларов на вклад 7% годовых с
  капитализацией 1 раз в месяц.
  Вывести в консоль, сможет ли он купить дом за 13500
  через 2 года после снятия вклада. И остаток после покупки.

  Итог = сумма + (1 + ставка в месяц не в %) ^ срок в месяцах.


*/

// === My Solutions ===
// const deposit = 12000;
// const monthlyRate = (deposit * 7) / 100;
// const housePrice = 13500;

// const totalEarnings = (deposit + monthlyRate) * (1 + monthlyRate) * 24;

// if (totalEarnings > housePrice) {
//   console.log(`Can buy. Left money:`, totalEarnings - housePrice);
// } else {
//   console.log("Can not buy.");
// }

const deposit = 10000;
const rate = 0.07;
const depositLength = 24;
const housePrice = 13500;
const result = deposit * (1 + rate / 12) ** depositLength;

if (result > housePrice) {
  console.log(
    `Total earnings: ${result}. Can buy! Left money: ${result - housePrice}`
  );
} else {
  console.log(`Total earnings: ${result}. Can not buy.`);
}
