// function logName(name) {
//   console.log(`You name is ${name}`);
// }

// logName("Mirlan");

// // Anonymous functions
// const sum = function (a, b) {
//   return a + b;
// };

// // console.log(sum(1, 2));

// // Arrow functions
// const addition = (a, b) => {
//   return a + b;
// };

// console.log(addition(100, 2));

// Task: rewrite function as an arrow function
// function toPower(num, power) {
//   const res  = num ** power;
//   return res;
// }

// Task Solution:
// const toPowerArrow = (num, power) => num ** power;

// Default function parameters

// function toPower(num, power = 2) {
//   const res = num ** power;
//   return res;
// }

// console.log(toPower(2, 3));
// console.log(toPower(2));

// function in function

const kgInUSD = 7;
const kmInUSD = 5;

function calculateW(present) {
  return present * kgInUSD;
}

function calculateKm(distance) {
  return distance * kmInUSD;
}

function getExchangePrice(present1, present2, distance) {
  const price1 = calculateW(present1);
  const price2 = calculateW(present2);
  const distancePrice = calculateKm(distance);

  return price1 + price2 + distancePrice;
}

console.log(getExchangePrice(1, 2, 10));
