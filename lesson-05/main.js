// Primitives

// number
const age = 10;
console.log(typeof age);

// string
const name = "John";
console.log(typeof name);

// boolean
const isAdult = true;
console.log(typeof isAdult);

// null
const person = null;
console.log(typeof person);

// undefined
const person2 = undefined;
console.log(typeof person2);

//symbol
const person3 = Symbol();
console.log(typeof person3);

//bigInt
const person4 =
  BigInt(
    100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
  );
console.log(typeof person4);
