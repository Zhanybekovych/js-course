// Assignment operators
let age = 20;
console.log(age);

// Assignment with addition
age += 10;
console.log(age);

// Assignment with subtraction
age -= 10;
console.log(age);

// Assignment with multiplication
age *= 10;
console.log(age);

// Assignment with division
age /= 10;
console.log(age);

// Assignment with modulus
age %= 10;
console.log(age);

// Assignment with exponentiation
age **= 10;
console.log(age);

// Increment
age++;
console.log(age);

// Decrement
age--;
console.log(age);

// Comparison operators
const a = 10;
const b = 20;
console.log(a > b);
console.log(a >= b);
console.log(a < b);
console.log(a <= b);
console.log(a == b);
console.log(a != b);
console.log(a === b);
