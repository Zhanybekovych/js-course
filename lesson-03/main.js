// this is a single line comment

/*
 * this is a multi line comment
 */

// Arithmetic operations

const a = 4;
const b = 10;

// addition

const sum = a + b;

// subtraction

const difference = a - b;

// multiplication

const product = a * b;

// division

const quotient = a / b;

// modulus

const modulus = a % b;

// power

const power = a ** b;

// concatenation
const city = "Bishkek";
const street = "Sovetstaya 15";
const address = city + street;
console.log(address);
