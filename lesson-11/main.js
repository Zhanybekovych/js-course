const role = "manager";

if (role === "manager") {
  console.log("manager");
} else if (role === "admin") {
  console.log("admin");
} else if (role === "ceo") {
  console.log("ceo");
} else {
  console.log("error");
}

switch (role) {
  case "manager":
    console.log("manager");
    break;
  case "admin":
    console.log("admin");
    break;
  case "ceo":
    console.log("ceo");
    break;
  default:
    console.log("error");
}

// grouping cases
switch (role) {
  case "manager":
  case "admin":
    console.log("Middle managment");
    break;
  case "seo":
    console.log("Top management");
    break;
  default:
    console.log("error");
}

const num = 1;

switch (true) {
  case num === 1:
    console.log("one");
    break;
  case num === 2:
    console.log("two");
    break;
  case num === 3:
    console.log("three");
    break;
  default:
    console.log("error");
}
