const bmw = 100000;
const ford = 10000;
const budget = 20000;

let message;

if (budget > bmw) {
  message = "Bmw";
} else {
  message = "Ford";
}

console.log(message);

console.log(budget > bmw ? "Bmw" : "Ford");
