/*
  Пользователь:
  - Возраст
  - Наличие работы
  - Деньги
  Нужно проверить может ли он купить новый Макбук за 2000 долларов?
  Он может брать не только свои деньги, но и взять кредит. Ему
  дадут 500 долларов, только если ему больше 24 лет и он имеет работу, 100 если ему просто больше 24 лет и 0 в ином случае.
  Напишите функцию, которая пронимает данные пользователя и товара и возвращает true или false

*/
// Solution
// function canBuy(age, hasJob, money) {
//   let credit = 0;
//   if (money + credit > 2000) {
//     return true;
//   } else if (age > 24 && hasJob) {
//     credit = 500;
//     if (money + credit > 2000) {
//       return true;
//     } else {
//       return false;
//     }
//   } else if (age > 24) {
//     credit = 100;
//     if (money + credit > 2000) {
//       return true;
//     } else {
//       return false;
//     }
//   }
// }

// console.log(canBuy(25, true, 1200));

function computeCredit(age, hasJob = false) {
  switch (true) {
    case age > 24 && hasJob:
      return 500;
    case age > 24:
      return 100;
    default:
      return 0;
  }
}

function canBuy(price, age, money, hasJob = false) {
  let credit = computeCredit(age, hasJob);

  return price <= credit + money;
}

console.log(canBuy(2000, 25, 1500, true));
