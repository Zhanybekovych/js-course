const money = 10;

if (money > 50) {
  console.log("Can buy");
} else if (money > 5) {
  console.log("Can buy cheaper");
} else {
  console.log("Can't buy");
}

console.log("Something else");
