/*
  Ваша часовая ставка 80$ и вы готовы работать не более
  5 часов в день 5 дней в неделю (кроем выходных).
  К вам приходит заказчик и предлагает заказ на 40
  часов работы.
  Сейчас понедельник.
  Вы должны уехать через 11 дней.
  Выведет в консоль:
  - Boolean переменную успеете ли вы взяться за работу
  - Сколько вы за нее попросите?
*/

// --- MY SOLUTION ---
/*
const rate = 80;
const projectHours = 40;
const workHoursPerDay = 5;

if (projectHours < workHoursPerDay * 11) {
  console.log(true);
  console.log(projectHours * rate);
} else {
  console.log(false);
}
*/

// MENTOR'S SOLUTION

const payRateUSD = 80;
const projectHours = 40;
const availableHours = (11 - 2) * 5;

console.log("Can Work?: " + (availableHours > projectHours));
console.log("Cost: " + payRateUSD * projectHours);
