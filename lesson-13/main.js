/*

  Методом promt получите ответ от пользоваетеля на
  вопрос "Сколько будет 7 + или - 15". Если ответ
  верен выведите в консоли "Успех" иначе "Вы робот", если
  введет ответ "Я не робот" тоже "Успех"

*/

// My solution

// const response = prompt("Please answer: 7 +(-) 15? ");

// if (response === "я не робот") {
//   document.write("Успех");
// } else if (Number(response) === 22) {
//   document.write("Успех");
// } else if (Number(response) === -7) {
//   document.write("Успех");
// } else {
//   document.write("Вы робот");
// }

const res = prompt("7 + (-) 15");

switch (true) {
  case res === "я не робот":
  case Number(res) === 22:
  case Number(res) === -8:
    document.write("Успех");
    break;
  default:
    document.write("Вы робот");
}

if (res === "Я не робот") {
  document.write("Успех");
} else {
  const resNum = Number(res);
  switch (resNum) {
    case -8:
    case 22:
      document.write("Успех");
      break;
    default:
      document.write("Вы робот");
  }
}
