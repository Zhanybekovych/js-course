/*
  Пользователь хочет проибрести игру в магазине
  Он может это сделать только если:
  - Его балланс больше 1000
  или число бонусов больше 100
  - Он не забанен
  - Игра не куплена
  - Игра в продаже
  Напишите условие для покупки и выведите в консоль
  результат

*/

const ballance = 1000;
const bonusBalance = 500;
const isBanned = true;
const isExist = false;
const isSelling = true;

// My solution
// if (
//   ballance > 1000 ||
//   (bonusBalance > 100 && isBanned && isExist && isSelling)
// ) {
//   console.log("Can buy");
// } else {
//   console.log("Can't buy");
// }
const canBuy =
  (ballance > 1000 || bonusBalance > 100) && !isBanned && !isExist && isSelling;

console.log(`Can buy: ${canBuy ? "yes" : "no"}`);
