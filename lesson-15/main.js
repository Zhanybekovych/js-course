const isAdmin = true;
const canWrite = true;

// AND
document.write(`System file ${isAdmin && canWrite} <br>`);

// OR
document.write(`Simple file ${isAdmin || canWrite} <br>`);

//NOT
document.write(`Invert ${!isAdmin} <br>`);

const isEditted = true;
const superAdmin = true;
document.write(
  `File edited: ${isAdmin && canWrite && (!isEditted || superAdmin)}<br>`
);

document.write("================================================<br>");

document.write("Vasya" || "Oleg");
document.write("<br>");
document.write(false || "Oleg");
document.write("<br>");

document.write("================================================<br>");

document.write("Vasya" && "Oleg");
document.write("<br>");
document.write(false && "Oleg");
document.write("<br>");
document.write("Vasya" && false);
document.write("<br>");

document.write("================================================<br>");
let a;
const userName = a || "Pete";
document.write(userName);

document.write("================================================<br>");
const fileName = isAdmin && "file.mp4";
document.write(fileName);
